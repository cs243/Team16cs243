﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Timer1 : MonoBehaviour {

	public Text timerText;
	private float startTime;
	private float total=30.0f;
	// Use this for initialization
	void Start () {
		startTime = Time.time;
	}
	
	
	void Update()
	{

			float t=total-(Time.time-startTime);
		string minutes = ((int)t / 60).ToString ();
		string seconds = (t % 60).ToString ("f4");
		if (t > 0)
			timerText.text = "Time Left: " + minutes + ":" + seconds;
		else
			timerText.text = "Time Up";
	}
	
	
	// Update is called once per frame
	
}

